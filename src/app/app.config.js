'use strict';

angular.module('ngDevstack')

.config(function ($urlRouterProvider) {

  $urlRouterProvider.otherwise('/main/home/');

  /*
  Make a trailing slash optional for all routes
   */
  $urlRouterProvider.rule(function ($injector, $location) {
    var path = $location.path(),
        search = $location.search(),
        params;

    if (path[path.length - 1] === '/') {
      return;
    }

    if (!Object.keys(search).length) {
      return path + '/';
    }

    params = [];
    angular.forEach(search, function (v, k) {
      params.push(k + '=' + v);
    });

    return path + '/?' + params.join('&');
  });
})

.run(function ($rootScope, $state, authenticationService, flashService) {
  
  var notAuthorisedUserStates = [
    'login',
    'signin'
  ];

  $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

    if (authenticationService.isLoggedIn()) {
      if (~notAuthorisedUserStates.indexOf(toState.name)) {
        event.preventDefault();
        flashService.show('Already logged in', 'info');
      }
    } else {
      if (!~notAuthorisedUserStates.indexOf(toState.name)) {
        event.preventDefault();
        console.log(fromState.name);
        $state.go(fromState.name || 'login');
      }
    }

  });

});
