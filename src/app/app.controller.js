'use strict';

angular.module('ngDevstack')

.controller('AppCtrl', function ($rootScope, $scope) {

  // handling UI Bootstrap Collapse plugin
  $scope.isCollapsed = true;

});
