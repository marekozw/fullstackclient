'use strict';

angular.module('ngDevstack', [
    'templates.app',

    'ngDevstack.conf',
    'ngDevstack.login',
    'ngDevstack.signin',
    'ngDevstack.main',
    
    'ngDevstack.authentication',
    'ngDevstack.flash',
    'ngDevstack.apiService',
    
    'ui.bootstrap',
    'ui.router'
]);
