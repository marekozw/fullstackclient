(function () {
  'use strict';

  var loginConfig = function ($stateProvider) {
    $stateProvider.state('login', {
      url: '/login/',
      views: {
        'main': {
          controller: 'LoginCtrl',
          templateUrl: 'login/login.tpl.html'
        }
      }  
    });

  };

  angular.module('ngDevstack.login')
    .config(loginConfig);
})();