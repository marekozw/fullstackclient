(function () {
  'use strict';

  var loginCtrl = function ($scope, $state, authenticationService) {

    $scope.credentials = {
      email: '',
      password: ''
    };

    $scope.login = function () {
      authenticationService.login($scope.credentials).then(function (res) {
        $state.go('main.home');
      });
    };

  };

  angular.module('ngDevstack.login')
    .controller('LoginCtrl', loginCtrl);

})();
