'use strict';

angular.module('ngDevstack.about')

.config(function ($stateProvider) {
    $stateProvider.state('main.about', {
        url: 'about/',
        views: {
            "main": {
                controller: 'AboutCtrl',
                templateUrl: 'main/about/about.tpl.html'
            }
        },
        data: {
            pageTitle: 'About'
        }
    });
});
