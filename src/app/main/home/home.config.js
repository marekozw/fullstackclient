'use strict';

angular.module('ngDevstack.home')

.config(function ($stateProvider) {
    $stateProvider.state('main.home', {
        url: 'home/',
        views: {
            "main": {
                controller: 'HomeCtrl',
                templateUrl: 'main/home/home.tpl.html'
            }
        },
        data: {
            pageTitle: 'Home'
        }
    });
});
