(function () {
  'use strict';

  var homeCtrl = function ($scope, api, restAuthorizationService) {
    $scope.isLoading = true;
    $scope.showUserData = false;
    $scope.loggedInUser = restAuthorizationService.loggedInUser;

    $scope.sendAjax = function () {
      $scope.showUserData = true;
      $scope.isLoading = true;

      api.getUsers().then(function (res) {
        console.log(res.data);
        $scope.user = res.data;
        $scope.isLoading = false;
      });

      api.getProducts().then(function (res) {
        console.log(res.data);
      });

    };

  };

  angular.module('ngDevstack.home')
    .controller('HomeCtrl', homeCtrl);
  
})();

