(function () {
  'use strict';

  var mainConfig = function ($stateProvider) {

    $stateProvider.state('main', {
      url: '/main/',
      views: {
        'main': {
          controller: 'MainCtrl',
          templateUrl: 'main/main.tpl.html'
        }
      }
    });

  };

  angular.module('ngDevstack.main')
    .config(mainConfig);

})();
