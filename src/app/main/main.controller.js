(function () {
  'use strict';

  var mainCtrl = function ($scope, $state, authenticationService) {
  
    $scope.logout = function () {
      authenticationService.logout().then(function () {
        $state.go('login');
      });
    };
    
  };

  angular.module('ngDevstack.main')
    .controller('MainCtrl', mainCtrl);

})();
