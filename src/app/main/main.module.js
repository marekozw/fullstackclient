(function () {
  'use strict';

  angular.module('ngDevstack.main', [
    'ui.router',

    'ngDevstack.home',
    'ngDevstack.about'
  ]);
  
})();
