(function () {
  'use strict';

  var signinConfig = function ($stateProvider) {
    $stateProvider.state('signin', {
      url: '/signin/',
      views: {
        'main': {
          controller: 'SigninCtrl',
          templateUrl: 'signin/signin.tpl.html'
        }
      }  
    });

  };

  angular.module('ngDevstack.signin')
    .config(signinConfig);
})();
