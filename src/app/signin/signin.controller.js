(function () {
  'use strict';

  var signinCtrl = function ($scope, $state, authenticationService, flashService) {

    $scope.credentials = {
      email: '',
      password: '',
      confirmPassword: ''
    };

    $scope.signin = function () {
      if ($scope.credentials.password !== $scope.credentials.confirmPassword) {
        flashService.show('Retype the same password.', 'warning');
        return;
      }

      authenticationService.signin($scope.credentials).then(function (res) {
        $state.go('main.home');
      });
    };

  };

  angular.module('ngDevstack.signin')
    .controller('SigninCtrl', signinCtrl);

})();
