(function (angular) {
  'use strict';

  var mpLoading = function () {
    return {
      restrict: 'A',
      replace: true,
      transclude: true,
      templateUrl: 'directives/mpLoading/mpLoading.tpl.html',
      scope: {
        loading: '@mpLoading',
        message: '@mpLoadingMessage'
      }
    };
  };

  angular.module('ngDevstack.myDirectiveModule', [])
    .directive('mpLoading', mpLoading);

})(angular);
