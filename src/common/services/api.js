(function () {
  'use strict';

  var api = function (restHttp) {
    var api = {

      getUsers: function () {
        return restHttp.get('/user');
      },

      getProducts: function () {
        return restHttp.get('/products');
      },

      login: function (credentials) {
        return restHttp.post('/auth/login', {
          email: credentials.email,
          password: credentials.password
        });
      },

      signin: function (credentials) {
        return restHttp.post('/auth/signin', {
          email: credentials.email,
          password: credentials.password
        });
      },

      logout: function () {
        return restHttp.get('/auth/logout');
      }

    };

    return api;
  };

  angular.module('ngDevstack.apiService', ['ngDevstack.restHttp'])
    .factory('api', api);

})();
