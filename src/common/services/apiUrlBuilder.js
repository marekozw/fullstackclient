(function () {
  'use strict';

  var SERVER_URL = 'http://localhost',
    SERVER_PORT = 5000,
    API_URL = '/api',
    FULL_URL = SERVER_URL + ':' + SERVER_PORT + API_URL;

  var apiUrlBuilder = function () {
    var apiUrlBuilder = function (url) {
      return FULL_URL + url;
    };

    return apiUrlBuilder;
  };

  angular.module('ngDevstack.apiUrlBuilderService', [])
    .factory('apiUrlBuilder', apiUrlBuilder);
})();
