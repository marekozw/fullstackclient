(function () {
  'use strict';

  var authenticationService = function ($http, $state, restAuthorizationService, api, flashService) {

    var onLoginSuccess = function (user) {
      restAuthorizationService.setUser(user);
      flashService.clear();
    };
    
    var onLoginFailure = function (message) {
      flashService.show(message, 'danger');
    };

    var authenticationService = {

      signin: function (credentials) {
        return api.signin(credentials).then(function (res) {
          var user = res.data;

          onLoginSuccess(user);
          return res;
        }, function (res) {
          onLoginFailure(res.data.flash);
          return res;
        });
      },

      login: function (credentials) {
        return api.login(credentials).then(function (res) {
          var user = res.data;

          onLoginSuccess(user);
          return res;
        }, function (res) {
          onLoginFailure(res.data.flash);
          return res;
        });
      },

      logout: function () {
        return api.logout().then(function (res) {
          restAuthorizationService.unsetUser();
          flashService.show(res.data.flash, 'info');

          return res;
        });
      },

      isLoggedIn: function () {
        return restAuthorizationService.isAuthorized();
      },

      getUser: function () {
        return restAuthorizationService.loggedInUser;
      }
      
    };

    return authenticationService;
  };

  angular.module('ngDevstack.authentication', ['ngDevstack.apiService', 'ngDevstack.restAuthorization'])
    .factory('authenticationService', authenticationService);

})();
