(function () {
  'use strict';

  var flashServiceFunction = function ($rootScope) {

    var flashService = {

      show: function (message, type) {
        $rootScope.flashType = type || 'info';
        $rootScope.flash = message;
      },

      clear: function () {
        $rootScope.flash = '';
      }

    };

    return flashService;
  };

  angular.module('ngDevstack.flash', [])
    .factory('flashService', flashServiceFunction);
})();
