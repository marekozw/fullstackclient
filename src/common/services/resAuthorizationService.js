(function () {
  'use strict';

  var restAuthorizationServiceFunction = function (sessionService) {
    var restAuthorization = {

      loggedInUser: null,

      setUser: function (user) {
        this.loggedInUser = user;
        sessionService.set('user', angular.toJson(user));
      },
      unsetUser: function () {
        this.loggedInUser = null;
        sessionService.unset('user');
      },
      restHeaders: function () {
        var headers = {
          email: this.loggedInUser ? this.loggedInUser.email : '',
          password: this.loggedInUser ? this.loggedInUser.password : ''
        };

        return angular.toJson(headers);
      },
      isAuthorized: function () {
        var cachedUser = angular.fromJson(sessionService.get('user'));

        this.loggedInUser = cachedUser;
        return !!this.loggedInUser;
      }

    };

    return restAuthorization;
  };

  angular.module('ngDevstack.restAuthorization', ['ngDevstack.session'])
    .factory('restAuthorizationService', restAuthorizationServiceFunction);
})();
