(function () {
  'use strict';

  var restHttp = function ($http, restAuthorizationService, apiUrlBuilder) {
    var http = {
      
      get: function (url, options) {
        return $http({
          method: 'GET',
          url: apiUrlBuilder(url),
          headers: {
            'x-credentials': restAuthorizationService.restHeaders()
          }
        });
      },

      post: function (url, data, options) {
        return $http({
          method: 'POST',
          url: apiUrlBuilder(url),
          data: data,
          headers: {
            'x-credentials': restAuthorizationService.restHeaders()
          }
        });
      }

    };

    return http;
  };

  angular.module('ngDevstack.restHttp', ['ngDevstack.apiUrlBuilderService'])
    .factory('restHttp', restHttp);
})();
