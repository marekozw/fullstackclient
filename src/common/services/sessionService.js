(function () {
  'use strict';

  var sessionServiceFunction = function () {

    var sessionService = {

      get: function (key) {
        return sessionStorage.getItem(key);
      },

      set: function (key, value) {
        return sessionStorage.setItem(key, value);
      },

      unset: function (key) {
        return sessionStorage.removeItem(key);
      }

    };

    return sessionService;
  };

  angular.module('ngDevstack.session', [])
    .factory('sessionService', sessionServiceFunction);
})();
